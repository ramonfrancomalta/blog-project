import React, {useContext} from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import {Feather} from '@expo/vector-icons'
import { Context } from '../context/BlogContext';

const ShowScreen =  ({navigation}) => {
    const {state} = useContext(Context);
    const blogPost = state.find((blogPost) => blogPost.id === navigation.getParam('id'));

    return (
        <View>
            <Text style = {styles.textTitleStyle}> {blogPost.title}</Text>
            <Text style = {styles.textContentStyle}> {blogPost.content}</Text>
        </View>
    );
};

ShowScreen.navigationOptions = ({navigation}) => {
    return{
        headerRight: () => 
            <TouchableOpacity onPress={() => navigation.navigate('Edit', {id: navigation.getParam('id')})}> 
                <Feather name= "edit" size = {30} />
            </TouchableOpacity>
    };
};


const styles = StyleSheet.create({
    titleStyle:{
        fontSize: 24,
        fontWeight: 'bold'
    },
    textTitleStyle: {
        fontSize: 15
    },
    textContentStyle: {

    }
});

export default ShowScreen;