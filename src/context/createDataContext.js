import React, {useReducer} from 'react';


export default (reducer, actions, initialState) =>{
    const Context = React.createContext();

    const Provider = ({children}) => {
        const [state, distpatch] = useReducer(reducer, initialState);

        const boundActions = {};

        for (let key in actions){
            boundActions[key] = actions[key](distpatch);
        }

        return ( 
            <Context.Provider value = {{state, ...boundActions}} >
                {children}
            </Context.Provider>
        );
    }


    return {Context, Provider};
};